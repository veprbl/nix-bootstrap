#!/bin/bash
# vim: set noexpandtab:

set -e
set -x

# install path
: ${NIX_PREFIX:=$(readlink -f "$HOME")} # path to nix store must not contain symlinks
# defaults to half available cores, change if needed
: ${NUM_THREADS:=$(($(nproc --all)/2))}

# directory to hold all files and where to build software
MYTMP=/tmp/nix-boot-$(whoami)
# use scratch, if present for faster filesystem access
if [ -d /scratch ]; then
	MYTMP=/scratch/nix-boot-$(whoami)
fi

rm -rf "$MYTMP"
mkdir "$MYTMP"
pushd "$MYTMP"

# sanitize paths to avoid conflicts/issues when building
export PATH=$MYTMP/bin:/bin:/usr/bin:/usr/sbin:/sbin
export LD_LIBRARY_PATH=$MYTMP/lib64:$MYTMP/lib
export LIBRARY_PATH=$MYTMP/lib64:$MYTMP/lib
export CPATH=$MYTMP/include
export PKG_CONFIG_PATH=$MYTMP/lib64/pkgconfig:$MYTMP/lib/pkgconfig:$MYTMP/share/pkgconfig

function mmi () {
	make -j "$NUM_THREADS" 1> /dev/null
	make install 1> /dev/null
}

LOCAL_GCC_MAJOR_VERSION=$(gcc --version | head -n1 | sed 's/^.* \([0-9]\)\..*/\1/g')
echo "Detected gcc ${LOCAL_GCC_MAJOR_VERSION}"
BUILD_GCC=
if [ "x$LOCAL_GCC_MAJOR_VERSION" = "x" ] || [ "$LOCAL_GCC_MAJOR_VERSION" -lt 12 ]; then
	BUILD_GCC=true
fi

GCC_VERSION=12.1.0
BROTLI_VERSION=1.0.7
BOOST_VERSION=1.68.0
BOOST_DIR_VERSION=${BOOST_VERSION//./_}
NIX_VERSION=2.8.1
SQLITE_VERSION=3.35.02
SQLITE_DIR_VERSION=${SQLITE_VERSION//./}00
LIBEDITLINE_VERSION=1.17.1
LIBARCHIVE_VERSION=3.6.1
LIBSODIUM_VERSION=1.0.18-stable
LIBCPUID_VERSION=0.5.1
BDWGC_VERSION=8.0.6
GTEST_VERSION=1.12.1
NLOHMANN_JSON_VERSION=3.11.2
LOWDOWN_VERSION=1_0_0
CURL_VERSION=7.85.0

if [ "$BUILD_GCC" = true ]; then
	wget http://ftpmirror.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.xz
fi
wget https://github.com/google/brotli/archive/v$BROTLI_VERSION.tar.gz
wget "https://boostorg.jfrog.io/artifactory/main/release/$BOOST_VERSION/source/boost_$BOOST_DIR_VERSION.tar.gz"
wget "https://github.com/NixOS/nix/archive/refs/tags/$NIX_VERSION.tar.gz"
wget "https://sqlite.org/2021/sqlite-autoconf-$SQLITE_DIR_VERSION.tar.gz"
wget "https://github.com/troglobit/editline/releases/download/$LIBEDITLINE_VERSION/editline-$LIBEDITLINE_VERSION.tar.xz"
wget "https://libarchive.org/downloads/libarchive-$LIBARCHIVE_VERSION.tar.xz"
wget "https://download.libsodium.org/libsodium/releases/libsodium-$LIBSODIUM_VERSION.tar.gz"
wget https://github.com/anrieff/libcpuid/archive/refs/tags/v$LIBCPUID_VERSION.tar.gz
wget https://github.com/ivmai/bdwgc/releases/download/v$BDWGC_VERSION/gc-$BDWGC_VERSION.tar.gz
wget https://github.com/google/googletest/archive/refs/tags/release-$GTEST_VERSION.tar.gz
wget https://github.com/nlohmann/json/archive/refs/tags/v$NLOHMANN_JSON_VERSION.tar.gz
wget https://github.com/kristapsdz/lowdown/archive/refs/tags/VERSION_$LOWDOWN_VERSION.tar.gz
wget https://curl.se/download/curl-$CURL_VERSION.tar.bz2

mkdir -p "$MYTMP/bin"
wget --no-clobber https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 -o "$MYTMP/bin/jq"
chmod +x "$MYTMP/bin/jq"

tar zxf "v$BROTLI_VERSION.tar.gz"
tar zxf "boost_$BOOST_DIR_VERSION.tar.gz"
tar zxf "$NIX_VERSION.tar.gz"
tar zxf "sqlite-autoconf-$SQLITE_DIR_VERSION.tar.gz"
tar Jxf "editline-$LIBEDITLINE_VERSION.tar.xz"
tar Jxf libarchive-$LIBARCHIVE_VERSION.tar.xz
tar zxf libsodium-$LIBSODIUM_VERSION.tar.gz
tar zxf v$LIBCPUID_VERSION.tar.gz
tar zxf gc-$BDWGC_VERSION.tar.gz
tar zxf release-$GTEST_VERSION.tar.gz
tar zxf v$NLOHMANN_JSON_VERSION.tar.gz
tar zxf VERSION_$LOWDOWN_VERSION.tar.gz
tar jxf curl-$CURL_VERSION.tar.bz2

if [ "$BUILD_GCC" = true ]; then
	tar Jxf gcc-$GCC_VERSION.tar.xz
	pushd gcc-$GCC_VERSION
	sed -i "s#ftp://gcc.gnu.org#https://gcc.gnu.org#g" ./contrib/download_prerequisites
	./contrib/download_prerequisites
	popd
	rm -rf objdir || true
	mkdir -p objdir
	pushd objdir
	"$PWD/../gcc-$GCC_VERSION/configure" --prefix="$MYTMP" --enable-languages=c,c++ --disable-multilib --disable-bootstrap 1>/dev/null 2>/dev/null
	mmi
	popd
fi

pushd "sqlite-autoconf-$SQLITE_DIR_VERSION"
./configure --prefix="$MYTMP" 1>/dev/null 2>/dev/null
mmi
popd

pushd editline-$LIBEDITLINE_VERSION
./configure --prefix="$MYTMP" 1>/dev/null 2>/dev/null
mmi
popd

pushd brotli-$BROTLI_VERSION
./configure-cmake --prefix="$MYTMP" 1>/dev/null 2>/dev/null
mmi
popd

pushd boost_"$BOOST_DIR_VERSION"
./bootstrap.sh --with-libraries=context --prefix="$MYTMP" 1>/dev/null 2>/dev/null
./b2 install 1>/dev/null 2>/dev/null
popd

pushd libarchive-$LIBARCHIVE_VERSION
LDFLAGS="-Wl,--copy-dt-needed-entries" ./configure --prefix="$MYTMP" 1>/dev/null 2>/dev/null
mmi
popd

pushd libsodium-stable
./configure --prefix="$MYTMP" 1>/dev/null 2>/dev/null
mmi
popd

pushd libcpuid-$LIBCPUID_VERSION
libtoolize
autoreconf --install
./configure --prefix="$MYTMP" 1>/dev/null 2>/dev/null
mmi
popd

pushd gc-$BDWGC_VERSION
./configure --prefix="$MYTMP" --enable-cplusplus --with-libatomic-ops=none --enable-mmap 1>/dev/null 2>/dev/null
mmi
popd

pushd googletest-release-$GTEST_VERSION
cmake -B build -S . -DCMAKE_INSTALL_PREFIX="$MYTMP"
cmake --build build
cmake --install build
popd

pushd json-$NLOHMANN_JSON_VERSION
cmake -B build -S . -DCMAKE_INSTALL_PREFIX="$MYTMP"
cmake --build build
cmake --install build
popd

pushd lowdown-VERSION_$LOWDOWN_VERSION
./configure PREFIX="$MYTMP"
sed -i Makefile -e '/^VALGRINDS\s\+!=/d' -e '/^VALGRINDDIFFS\s\+!=/d'
make
make install install_libs
popd

pushd curl-$CURL_VERSION
./configure --prefix="$MYTMP" --with-openssl 1>/dev/null 2>/dev/null
mmi
popd

pushd "nix-$NIX_VERSION"
sed -i Makefile -e 's#doc/manual/local.mk##'
./bootstrap.sh
./configure \
	--disable-seccomp-sandboxing \
	--prefix="$MYTMP" \
	--with-boost="$MYTMP" \
	--with-store-dir="$NIX_PREFIX/nix/store" \
	--localstatedir="$NIX_PREFIX/nix/var"
mmi
popd

if [ ! -d ~/nixpkgs ]; then
	git clone -b release-20.09 https://github.com/NixOS/nixpkgs.git ~/nixpkgs
fi

if [ ! -d ~/.config/nix ]; then
	mkdir -p ~/.config/nix
fi

touch ~/.config/nix/nix.conf
grep -q "sandbox\s*=\s*false" ~/.config/nix/nix.conf ||
	echo "sandbox = false" >> ~/.config/nix/nix.conf

if [ ! -L ~/.nix-profile ]; then
	ln -s "$NIX_PREFIX/nix/var/nix/profiles/default" ~/.nix-profile
fi

if [ ! -d ~/.config/nixpkgs/overlays/nix-bootstrap ]; then
	mkdir -p ~/.config/nixpkgs/overlays/nix-bootstrap
fi

cat <<EOF > ~/.config/nixpkgs/overlays/nix-bootstrap/default.nix
final: prev: {
  nix = prev.nix.override {
    storeDir = "$NIX_PREFIX/nix/store";
    stateDir = "$NIX_PREFIX/nix/var";
  };

  gitMinimal = prev.gitMinimal.overrideAttrs (_: { doInstallCheck = false; });
}
EOF

# use nix to bootstrap stdenv and install proper nix
nix-env -Q -j 2 --cores "$NUM_THREADS" -iA nix -f ~/nixpkgs

rm -rf "$MYTMP"
set +x

cat <<EOF


============================================

Add following to your shell rc file:

export NIX_PATH=nixpkgs=\$HOME/nixpkgs
source ~/.nix-profile/etc/profile.d/nix.sh

============================================

EOF
