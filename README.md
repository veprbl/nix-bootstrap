This repository contains a script that allows to bootstrap a single-user installation of [nix](http://nixos.org/nix/). It currently only targets RHEL7 based systems (CentOS 7, Scientific Linux 7).

This will place store in ```$HOME/nix/store``` instead of ```/nix/store```. The drawback of this is that you then will not be able to use the official Hydra binary cache since store paths are not rellocable.
